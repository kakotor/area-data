package org.fireye;

import com.google.common.base.CharMatcher;

/**
 * Created by bysun on 2015/4/12.
 */
public class Model{

    /**
     * 编码
     */
    private String code;
    /**
     * 名称
     */
    private String name;
    /**
     * 级别
     */
    private Integer level;
    /**
     * 父编码
     */
    private String parentCode;

    public Model() {
    }

    public Model(String[] d) {
        this.code = CharMatcher.WHITESPACE.trimFrom(d[0]);
        int codeInt = Integer.parseInt(code);
        this.code = CharMatcher.WHITESPACE.trimFrom(d[0]);
        this.name = CharMatcher.WHITESPACE.trimFrom(d[1]);
        if(0 == codeInt%10000){
            level = 1;
        }else if (0 == codeInt%100){
            level = 2;
            parentCode = String.valueOf(codeInt - (codeInt%10000));

        }else{
            level = 3;
            parentCode = String.valueOf(codeInt - (codeInt%100));
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }
}
