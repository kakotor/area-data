package org.fireye;

import com.google.common.base.Strings;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 获取国统的 省市区县
 */
public class App {
    /**
     * main
     *
     * @param args 参数
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Document doc = Jsoup.connect("http://www.stats.gov.cn/tjsj/tjbz/xzqhdm/201401/t20140116_501070.html").get();
        List<Model> list = doc.select("p.MsoNormal")
            .stream()
            .filter(element -> element.hasText() && !Strings.isNullOrEmpty(element.text()))
            .map(mapper -> mapper.text().split("\\s+"))
            .filter(strs -> strs.length == 2)
            .map(Model::new)
            .collect(Collectors.toList());
        String sql = "INSERT INTO {0} (area_code,area_name,area_level,parent_code) VALUES(''{1}'',''{2}'',{3},''{4}'');";
        list.forEach(model ->
                System.out.println(
                    MessageFormat.format(
                        sql,
                        "areas",
                        model.getCode(),
                        model.getName(),
                        model.getLevel(),
                        model.getParentCode()
                    )
                )
        );
    }
}
